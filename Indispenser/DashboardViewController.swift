//
//  FirstViewController.swift
//  Indispenser
//
//  Created by Milo Gilad on 11/26/18.
//  Copyright © 2018 NBPS. All rights reserved.
//

import UIKit

class DashboardViewController: UIViewController {

    @IBOutlet weak var caloricEntry: UITextField!
    @IBOutlet weak var dispenseButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        dispenseButton.makeRound(); cancelButton.makeRound();
        
    }

    @IBAction func dispensePress(_ sender: Any) {
        self.view.endEditing(true);
    }
    
    @IBAction func cancelPress(_ sender: Any) {
        self.view.endEditing(true);
    }
    
}

