//
//  RoundButton.swift
//  Indispenser
//
//  Created by Milo Gilad on 11/27/18.
//  Copyright © 2018 NBPS. All rights reserved.
//

import UIKit

extension UIButton {
    func makeRound() {
        self.layer.cornerRadius = 10;
        self.clipsToBounds = true;
    }
}
